//Playing Cards
//Aaliyah Brooks


#include <iostream>
#include <conio.h>


using namespace std;


// enums for rank and suit
enum Rank
{
	Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace
};
enum Suit
{
	Spades, Hearts, Diamonds, Clubs
};

// struct for card
struct Card
{
	Rank rank;
	Suit suit;
};
void PrintCard(Card card)
{
	switch (card.rank)
	{
	case Ace:
		cout << "The Ace of ";
		break;
	case King:
		cout << "The King of ";
		break;
	case Queen:
		cout << "The Queen of ";
		break;
	case Jack:
		cout << "The Jack of ";
		break;
	case Ten:
		cout << "The Ten of ";
		break;
	case Nine:
		cout << "The Nine of ";
		break;
	case Eight:
		cout << "The Eight of ";
		break;
	case Seven:
		cout << "The Seven of ";
		break;
	case Six:
		cout << "The Six of ";
		break;
	case Five:
		cout << "The Five of ";
		break;
	case Four:
		cout << "The Four of ";
		break;
	case Three:
		cout << "The Three of ";
		break;
	case Two:
		cout << "The Two of ";
		break;
	}
	switch (card.suit) 
	{
	case Spades:
		cout << "Spades";
		break;
	case Hearts:
		cout << "Hearts";
		break;
	case Diamonds:
		cout << "Diamonds";
		break;
	case Clubs:
		cout << "Clubs";
		break;

	}

};
Card HighCard(Card card1, Card card2)
{

	if (card1.rank > card2.rank) 
	{
		return card1;
	}
	else
	{
		return card2;
	}

	//shouldn't print anything to print console
}
int main()
{
	
	
	

	Card a;
	a.rank = Ace;
	a.suit = Hearts;
	PrintCard(a);

	Card b;
	b.rank = Ace;
	b.suit = Spades;

	Card c;
	c.rank = Jack;
	c.suit = Diamonds;

	Card d;
	d.rank = Nine;
	d.suit = Clubs;

	
	

	//PrintCard(HighCard(c, d));
	


	(void)_getch();
	return 0;
}

